name := """test-mongo-driver"""
organization := "fr.evertrust"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala, BuildInfoPlugin).settings(
  buildInfoKeys := Seq[BuildInfoKey](name, version, scalaVersion, sbtVersion),
  buildInfoKeys += BuildInfoKey.action("buildTime") {
    System.currentTimeMillis()
  }
)
scalaVersion := "2.13.14"

libraryDependencies ++= Seq(
  guice,
  "ai.x" %% "play-json-extensions" % "0.42.0",
  "org.mongodb.scala" %% "mongo-scala-driver" % "5.1.0"
)

