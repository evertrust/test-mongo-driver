package models.common

import org.bson.{BsonReader, BsonWriter}
import org.bson.codecs.{Codec, DecoderContext, EncoderContext}
import org.bson.codecs.configuration.{CodecProvider, CodecRegistry}

class EnumerationCodecProvider(enumeration: Enumeration) extends CodecProvider {

  def enumerationCodec[E <: Enumeration](enumeration: E): Codec[E#Value] = new Codec[E#Value] {
    override def decode(reader: BsonReader, decoderContext: DecoderContext): E#Value =
      enumeration.withName(reader.readString())

    override def encode(writer: BsonWriter, value: E#Value, encoderContext: EncoderContext): Unit =
      writer.writeString(value.toString)

    override def getEncoderClass: Class[E#Value] = classOf[E#Value]
  }

  def isEnumVal[T](clazz: Class[T]): Boolean = {
    clazz.getName.equals("scala.Enumeration$Val") ||
      clazz.getName.equals("scala.Enumeration$Value")
  }

  override def get[T](clazz: Class[T], registry: CodecRegistry): Codec[T] = {
    if (isEnumVal(clazz)) {
      enumerationCodec(enumeration).asInstanceOf[Codec[T]]
    } else {
      null
    }
  }

}
