package models.common

import org.mongodb.scala.bson.ObjectId
import play.api.libs.json.{JsError, JsString, JsSuccess, Reads, Writes}

import scala.util.{Failure, Success, Try}

object ObjectIdFormat {
  implicit val objectIdWrites: Writes[ObjectId] = (id: ObjectId) => JsString(id.toHexString)
  implicit val objectIdReads: Reads[ObjectId] = {
    case JsString(value) => Try(new ObjectId(value)) match {
      case Success(id) => JsSuccess(id)
      case Failure(_) => JsError(s"Invalid ObjectId '$value'")
    }
    case _ => JsError("Expecting ObjectId as hex string")
  }
}
