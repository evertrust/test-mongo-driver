package models.common

import org.bson.{BsonReader, BsonType}

import scala.annotation.tailrec

trait WithEnumDiscriminator {

  @tailrec
  private def findDiscriminator[E <: Enumeration](reader: BsonReader, key: String, e: E): E#Value = {
    reader.readBsonType() match {
      case BsonType.END_OF_DOCUMENT => throw new IllegalStateException(s"Unable to find discriminator value for key '$key'")
      case BsonType.STRING =>
        if (reader.readName().equals(key)) {
          e.withName(reader.readString())

        } else {
          reader.skipValue()
          findDiscriminator(reader, key, e)
        }
      case _ =>
        reader.skipName()
        reader.skipValue()
        findDiscriminator(reader, key, e)
    }
  }

  protected def getDiscriminator[E <: Enumeration](reader: BsonReader, key: String, e: E): E#Value = {
    // Setting a mark to rewind the reader at the very beginning
    val mark = reader.getMark
    // Starting to read the document
    reader.readStartDocument()
    // Looking for the discriminator value
    val value = findDiscriminator(reader, key, e)
    // Rewinding the reader
    mark.reset()
    // Returning the discriminator value
    value
  }

}
