package models.account

import models.common.{EnumerationCodecProvider, WithEnumDiscriminator}
import models.group.Group
import org.bson.codecs.configuration.{CodecRegistries, CodecRegistry}
import org.bson.{BsonReader, BsonWriter}
import org.bson.codecs.{Codec, DecoderContext, EncoderContext}
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.codecs.Macros

class AccountCodec extends Codec[Account] with WithEnumDiscriminator {

  private val codecRegistry: CodecRegistry = CodecRegistries.fromRegistries(
    CodecRegistries.fromProviders(
      new EnumerationCodecProvider(AccountType),
      Macros.createCodecProviderIgnoreNone[Group](),
      Macros.createCodecProviderIgnoreNone[AdministratorAccount](),
      Macros.createCodecProviderIgnoreNone[UserAccount]()
    ),
    DEFAULT_CODEC_REGISTRY
  )

  override def encode(writer: BsonWriter, value: Account, encoderContext: EncoderContext): Unit = {
    value match {
      case administrator: AdministratorAccount => codecRegistry.get(classOf[AdministratorAccount]).encode(writer, administrator, encoderContext)
      case user: UserAccount => codecRegistry.get(classOf[UserAccount]).encode(writer, user, encoderContext)
      case other => throw new IllegalArgumentException(s"Invalid account object '${other.getClass.getSimpleName.stripSuffix("$")}'")
    }
  }

  override def getEncoderClass: Class[Account] = classOf[Account]

  override def decode(reader: BsonReader, decoderContext: DecoderContext): Account = {
    getDiscriminator(reader, "type", AccountType) match {
      case AccountType.Administrator => codecRegistry.get(classOf[AdministratorAccount]).decode(reader, decoderContext)
      case AccountType.User => codecRegistry.get(classOf[UserAccount]).decode(reader, decoderContext)
      case other => throw new IllegalArgumentException(s"Invalid account type '$other'")
    }
  }

}
