package models.account

import models.account.AccountType.AccountType
import models.group.Group
import org.mongodb.scala.bson.ObjectId
import play.api.libs.json.{Format, JsError, JsResult, JsValue, Json}

import java.time.Instant

trait Account {

  val id: ObjectId
  val `type`: AccountType
  val login: String
  //val groups: Option[Seq[Group]]
  val creationDate: Instant

  //override def toString: String = s"type: '${`type`}', login: '$login', groups: ${groups.map(_.mkString("'", "', '", "'")).getOrElse("-")}, creationDate: '$creationDate'"

  def copyWithIdAndCreationDate(source: Account): Account

}

object Account {

  implicit val accountFormat: Format[Account] = new Format[Account] {
    override def reads(json: JsValue): JsResult[Account] = (json \ "type").as[AccountType] match {
      case AccountType.Administrator => json.validate[AdministratorAccount]
      case AccountType.User => json.validate[UserAccount]
      case other => JsError(s"Unsupported Account type '$other'")
    }

    override def writes(account: Account): JsValue = account match {
      case administrator: AdministratorAccount => Json.toJson(administrator)
      case user: UserAccount => Json.toJson(user)
      case other => throw new IllegalArgumentException(s"Unsupported Account type '${other.`type`}'")
    }
  }

}
