package models.account

import play.api.libs.json.{Format, Json}

object AccountType extends Enumeration {

  type AccountType = Value

  val Administrator: Value = Value("administrator")
  val User: Value = Value("user")

  implicit val accountTypeFormat: Format[AccountType.Value] = Json.formatEnum(this)
}
