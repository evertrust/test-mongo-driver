package models.account

import modules.mongodb.MongoDbApi
import org.bson.codecs.configuration.{CodecRegistries, CodecRegistry}
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.model.Filters.equal
import org.mongodb.scala.model.{IndexModel, IndexOptions, Indexes}
import org.mongodb.scala.{MongoCollection, result}

import javax.inject.{Inject, Singleton}
import scala.concurrent.{ExecutionContext, Future}

@Singleton
class AccountService @Inject() (implicit ec: ExecutionContext, mongoDbApi: MongoDbApi){

  private val accountCodecRegistry: CodecRegistry = CodecRegistries.fromRegistries(
    CodecRegistries.fromProviders(
      CodecRegistries.fromCodecs(new AccountCodec),
    ),
    DEFAULT_CODEC_REGISTRY
  )

  private val indexes = Seq(
    IndexModel(Indexes.ascending("login"), IndexOptions().name("login_idx").unique(true))
  )

  private val accountCollection: Future[MongoCollection[Account]]= {
    val collection: MongoCollection[Account] = mongoDbApi.db(accountCodecRegistry).getCollection("accounts")
    collection.createIndexes(indexes).toFuture() map { _ => collection }
  }


  def add(account: Account): Future[result.InsertOneResult] = {
    accountCollection.flatMap(_.insertOne(account).toFuture())
  }

  def delete(login: String): Future[result.DeleteResult] = {
    accountCollection.flatMap(_.deleteOne(equal("login", login)).toFuture())
  }

  def get(login: String): Future[Option[Account]] = {
    accountCollection.flatMap(_.find[Account](equal("login", login)).limit(1).toFuture() map { _.headOption})
  }

  def list(): Future[Seq[Account]] = {
    accountCollection.flatMap(_.find().toFuture())
  }

  def update(account: Account): Future[result.UpdateResult] = {
    accountCollection.flatMap(_.replaceOne(equal("_id", account.id), account).toFuture())
  }

}
