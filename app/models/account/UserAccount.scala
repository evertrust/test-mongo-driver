package models.account

import ai.x.play.json.Jsonx
import ai.x.play.json.Encoders.encoder
import models.account.AccountType.AccountType
import models.group.Group
import org.mongodb.scala.bson.ObjectId
import org.mongodb.scala.bson.annotations.BsonProperty
import play.api.libs.json.Format

import java.time.Instant

case class UserAccount(
  @BsonProperty("_id") id: ObjectId = new ObjectId(),
  `type`: AccountType = AccountType.User,
  login: String,
  groups: Option[Seq[Group]] = None,
  creationDate: Instant = Instant.now()
) extends Account {
  override def copyWithIdAndCreationDate(source: Account): Account = this.copy(id = source.id, creationDate = source.creationDate)
}

object UserAccount {
  import models.common.ObjectIdFormat._
  implicit val userAccountFormat: Format[UserAccount]  = Jsonx.formatCaseClassUseDefaults[UserAccount]
 }