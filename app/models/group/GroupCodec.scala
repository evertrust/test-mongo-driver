package models.group

import org.bson.{BsonReader, BsonWriter}
import org.bson.codecs.{Codec, DecoderContext, EncoderContext}

import scala.annotation.tailrec

object GroupCodec extends Codec[Group] {

  override def decode(reader: BsonReader, decoderContext: DecoderContext): Group = {
    reader.readStartDocument()
    val name = reader.readString("name")
    val displayName = reader.readString("displayName")
    reader.readEndDocument()
    Group(name, displayName)
  }

  override def encode(writer: BsonWriter, value: Group, encoderContext: EncoderContext): Unit = {
    writer.writeStartDocument()
    writer.writeString("name", value.name)
    writer.writeString("displayName", value.displayName)
    writer.writeEndDocument()
  }

  override def getEncoderClass: Class[Group] = classOf[Group]

  def decodeGroups(reader: BsonReader, decoderContext: DecoderContext): Seq[Group] = {
    @tailrec
    def readArray(reader: BsonReader, decoderContext: DecoderContext, acc: Seq[Group]): Seq[Group] = {
      reader.readBsonType() match {
        case org.bson.BsonType.END_OF_DOCUMENT => acc
        case _ =>
          val updated = acc :+ decode(reader, decoderContext)
          readArray(reader, decoderContext, updated)
      }
    }
    reader.readStartArray()
    val groups = readArray(reader, decoderContext, Seq.empty[Group])
    reader.readEndArray()
    groups
  }

  def decodeOptionGroups(reader: BsonReader, decoderContext: DecoderContext): Option[Seq[Group]] = {
    reader.getCurrentBsonType match {
      case org.bson.BsonType.NULL =>
        reader.readNull()
        None
      case _ => Some(decodeGroups(reader, decoderContext))
    }
  }

}
