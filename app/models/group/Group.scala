package models.group

import play.api.libs.json.{Json, OFormat}

case class Group(name: String, displayName: String)

object Group {
  implicit val groupFormat: OFormat[Group] = Json.format[Group]
}
