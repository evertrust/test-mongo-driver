package modules.mongodb

import buildinfo.BuildInfo
import com.google.inject.Inject
import org.bson.codecs.configuration.CodecRegistry
import org.mongodb.scala.{ConnectionString, MongoClient, MongoClientSettings, MongoDatabase}
import play.api.Configuration
import play.api.inject.ApplicationLifecycle

import javax.inject.Singleton
import scala.concurrent.Future
import scala.util.Try

@Singleton
class MongoDbApi @Inject()(lifecycle: ApplicationLifecycle, configuration: Configuration) {

  private val mongoDbUri = ConnectionString(configuration.get[String]("mongodb.uri"))
  private val mongoClientSettings = MongoClientSettings.builder()
    .applyConnectionString(mongoDbUri)
    .applicationName(s"${BuildInfo.name}/${BuildInfo.version}")
    .build()
  private val mongoClient: MongoClient = MongoClient(mongoClientSettings)

  lifecycle.addStopHook { () =>
    Future.successful(Try(mongoClient.close()))
  }

  private def db(codecOpt: Option[CodecRegistry] = None): MongoDatabase = {
    val mongoDb = mongoClient.getDatabase(mongoDbUri.getDatabase)
    codecOpt match {
      case Some(codec) => mongoDb.withCodecRegistry(codec)
      case None => mongoDb
    }
  }

  def db(codecRegistry: CodecRegistry): MongoDatabase = db(Some(codecRegistry))
  def db: MongoDatabase = db(None)

}
