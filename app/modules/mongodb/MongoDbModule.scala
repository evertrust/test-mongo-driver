package modules.mongodb

import com.google.inject.AbstractModule

class MongoDbModule extends AbstractModule {

  override def configure(): Unit = {
    bind(classOf[MongoDbApi]).asEagerSingleton()
  }

}
