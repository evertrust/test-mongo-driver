package controllers.api

import models.account.{Account, AccountService}
import play.api.Logging
import play.api.libs.json.{JsError, JsSuccess, JsValue, Json}
import play.api.mvc.{AbstractController, Action, AnyContent, ControllerComponents}

import javax.inject.{Inject, Singleton}
import scala.concurrent.{ExecutionContext, Future}

@Singleton
class AccountApiV1Controller @Inject() (cc: ControllerComponents)(implicit ec: ExecutionContext, accountService: AccountService) extends AbstractController(cc) with Logging {

  def list(): Action[AnyContent] = Action.async {
    accountService.list() map { accounts =>
      logger.error(s"JJWH: ${accounts.size}")
      if (accounts.isEmpty)
        NoContent
      else
        Ok(Json.toJson(accounts))
    } recover { t =>
      logger.error("Unable to list accounts", t)
      InternalServerError(t.getMessage)
    }
  }

  def get(login: String): Action[AnyContent] = Action.async {
    accountService.get(login) map {
      case Some(account) => Ok(Json.toJson(account))
      case None => NotFound
    } recover { t =>
      logger.error(s"Unable to get Account '$login'", t)
      InternalServerError(t.getMessage)
    }
  }

  def add(): Action[JsValue] = Action.async(parse.json) { implicit request =>
    request.body.validate[Account] match {
      case JsSuccess(account, _) => accountService.get(account.login) flatMap {
        case Some(_) => Future.successful(BadRequest(s"Account with login '${account.login}' already exists..."))
        case None => accountService.add(account) map { _ =>
          Created(Json.toJson(account))
        } recover { t =>
          logger.error(s"Unable to add Account '$account'", t)
          InternalServerError(t.getMessage)
        }
      }
      case e: JsError =>
        logger.error(s"Unable to add Account: $e")
        Future.successful(BadRequest(e.toString))
    }
  }

  def delete(login: String): Action[AnyContent] = Action.async {
    accountService.get(login) flatMap {
      case Some(_) =>
        accountService.delete(login) map { _ =>
          NoContent
        } recover { t =>
          logger.error(s"Unable to delete Account '$login'", t)
          InternalServerError(t.getMessage)
        }
      case None => Future.successful(NotFound)
    }
  }

  def update(): Action[JsValue] = Action.async(parse.json) { implicit request =>
    request.body.validate[Account] match {
      case JsSuccess(account, _) => accountService.get(account.login) flatMap {
        case Some(existingAccount) =>
          val updatedAccount = account.copyWithIdAndCreationDate(existingAccount)
          accountService.update(updatedAccount) map { _ =>
          Ok(Json.toJson(updatedAccount))
        }
        case None => Future.successful(NotFound)
      } recover { t =>
        logger.error(s"Unable to update Account '$account'", t)
        InternalServerError(t.getMessage)
      }
      case e: JsError => Future.successful(BadRequest(e.toString))
    }
  }

}
